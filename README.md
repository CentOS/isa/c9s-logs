# Logs from c9s build failures

The `logs` subdirectory contains logs from failed builds submitted for
CentOS 9 Stream on Stream Koji (not CBS Koji).  The logs can be used
to check if a failure in CBS Koji matches a failure in Stream Koji,
which means that it is likely a generic issue, and not something
specific to the ISA SIG.  The failing architecture in c9s may not be
x86_64 (if x86_64 succeeded); for each SRPM, at least one failure log
is included.
